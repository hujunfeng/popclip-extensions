# PopClip-Extensions

A few [PopClip](http://pilotmoon.com/popclip/) extensions that I created for myself. Please feel free to use them if you like.

## Open In Pivotal Tracker

This extension lets you select a story ID and open the story in Pivotal Tracker in your default browser.

## Uncrustify Selected Lines in Xcode

It requires [BBUncrustifyPlugin-Xcode](https://github.com/benoitsan/BBUncrustifyPlugin-Xcode), and you must create a keyboard shortcut ⌃⌥⇧⌘U for the menu command "Uncrustify Selected Lines" in Xcode.
